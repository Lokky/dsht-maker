yargs = require 'yargs'

yargs.options
    root:
        alias: 'r'
        desc : 'The root folder of the sources'
        type : 'string'

    host:
        alias: 'h'
        desc : 'API Host URL'
        type : 'string'

argv = yargs.argv
argv.root ?= './'
argv.host ?= 'localhost:8081'

module.exports = argv
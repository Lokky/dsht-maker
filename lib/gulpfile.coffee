# Gulp config
# ===========

gulp       = require 'gulp'
concat     = require 'gulp-concat'
order      = require 'gulp-order'
nganno     = require 'gulp-ng-annotate'
uglify     = require 'gulp-uglify'
estream    = require 'event-stream'
rename     = require 'gulp-rename'
replace    = require 'gulp-replace'
bowerFiles = require 'main-bower-files'
cleancss   = require 'gulp-clean-css'
sass       = require 'gulp-sass'
del        = require 'del'

# Main tasks
# ----------

# Clean and build
gulp.task 'default', ['clean'], -> gulp.start 'build'

# Build all sources
gulp.task 'build', [
    'js:vendor'
    'js:foot'
    'js:app'
    'css:vendor'
    'css:app'
    'htm:index'
    'htm:template'
]

# Build and rebuild when sources were changed
gulp.task 'building', ['build'], ->
    console.log "Start watching #{argv.root}"
    watcher = gulp.watch [
        "#{argv.root}/src/**/*.*"
        "#{argv.root}/bower.json"
    ], ['build']
    watcher.on 'change', (event) -> console.log 'File ' + event.path + ' was ' + event.type + ', running tasks...'

# Clean all generated files
gulp.task 'clean', (resolveCallback) ->
    del.sync(
        [ "#{argv.root}/out" ]
        force: yes
        (err) -> terminate 500, 'Clean-task was failed:', err if err
    )
    resolveCallback()

# Secondary tasks for special aims
# --------------------------------

# #### JS sources
gulp.task 'js:vendor', ->
    {vendors, orderList} = getVendors '**/*.js'
    gulp.src vendors
    .pipe order orderList
    .pipe logStreamPipe 'VENDOR'
    .pipe concat 'vendor.js'
    .pipe nganno()
    .pipe uglify()
    .pipe gulp.dest "#{argv.root}/out/js"

gulp.task 'js:foot', ->
    gulp.src [ "#{argv.root}/src/js/**/*.js" ]
    .pipe logStreamPipe 'JS FOOT'
    .pipe concat 'foot.js'
    .pipe uglify()
    .pipe gulp.dest "#{argv.root}/out/js"

gulp.task 'js:app', ->
    gulp.src [ "#{argv.root}/src/app/**/*.js" ]
    .pipe logStreamPipe 'APP'
    .pipe replace /__api_host__/, argv.host
    .pipe rename (path) -> path.dirname = ''
    .pipe gulp.dest "#{argv.root}/out/app"

# #### HTML sources
gulp.task 'htm:index', ->
    gulp.src [
        "#{argv.root}/src/html/*.*"
    ]
    .pipe logStreamPipe 'HTML INDEX'
    .pipe gulp.dest "#{argv.root}/out"

gulp.task 'htm:template', ->
    gulp.src [
        "#{argv.root}/src/htm/**/*.*"
    ]
    .pipe logStreamPipe 'HTML TEMPLATE'
    .pipe gulp.dest "#{argv.root}/out/htm"

# #### CSS sources
gulp.task 'css:vendor', ->
    {vendors, orderList} = getVendors '**/*.css'
    gulp.src vendors
    .pipe order orderList
    .pipe logStreamPipe 'VENDOR CSS'
    .pipe concat 'vendor.css'
    # .pipe cleancss()
    .pipe gulp.dest "#{argv.root}/out/css"

gulp.task 'css:app', ->
    gulp.src [
        "#{argv.root}/src/css/*.{scss,sass}"
        "!#{argv.root}/src/css/_*.*"
    ]
    .pipe sass()
    .pipe logStreamPipe 'APP CSS'
    .pipe concat 'app.css'
    .pipe cleancss()
    .pipe gulp.dest "#{argv.root}/out/css"

# #### Private implementation

getVendors = (filter) ->
    installVendorsOnce()

    vendors = bowerFiles
        paths:
            bowerDirectory : "#{argv.root}/vendor"
            bowerJson      : "#{argv.root}/bower.json"
        filter: filter

    orderList = vendors.map (vendorPath) ->
        [..., filename] = vendorPath.split '/'
        "**/#{filename}"

    {vendors, orderList}

vendorsInstalled = no
installVendorsOnce = ->
    return if vendorsInstalled
    runShell 
        cmd: 'bower'
        params: ['install', "--config.cwd=#{argv.root}"]
        caption: 'Install dependencies with Bower'
    vendorsInstalled = yes

logStreamPipe = (aim) -> estream.map (file, cb) ->
    console.log "#{aim}#{getSpacer 15, aim.length, '_'}FILE:#{file.path}"
    cb null, file

getSpacer = (tab, len = 0, ch = ' ', s = '') ->
    s += ch while s.length < tab - len
    s

runShell = (options) ->
    {cmd, params, caption} = options
    getCommandStr = -> ([cmd].concat params).join ' '
    console.log 'RUNSHELL:', caption ? getCommandStr()
    childProcess = (require 'child_process').spawnSync cmd, params, stdio: [0, 1, 2]
    if childProcess.status is 0
        console.log "#{caption ? getCommandStr()} --> SUCCESS"
    else
        terminate 500, "#{getCommandStr()} --> #{childProcess.status}"

terminate = (code, logs...) ->
    console.log "TERMINATED:", logs... if 1 <= logs.length
    process.exit code ? 500
// Use CoffeeScript
require('coffee-script/register');

// Get command-line options
argv = require('./lib/argv');

// Go on
require('./lib/gulpfile');